'use strict';

var scotchApp = angular.module('afishaApp', ['ngRoute']);

scotchApp
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'pages/wait.html'
            })
            // .when('/admin-signup', {
            //     templateUrl: 'pages/admin-signup.html'
            // })
            .when('/add-app', {
                templateUrl: 'pages/add-app.html'
            })
            .when('/empty', {
                templateUrl: 'pages/empty.html'
            })
            .when('/create-event', {
                templateUrl: 'pages/create-event.html'
                // controller: 'contactController'
            })
            .when('/play', {
                templateUrl: 'pages/match-play.html',
                controller: 'matchPlayController'
            })
            .when('/matches', {
                templateUrl: 'pages/matches.html',
                controller: 'matchController'
            })
            .when('/user-signup', {
                templateUrl: 'pages/user-signup.html'
            });
    });

scotchApp.controller('mainController', function ($scope, $location, $rootScope) {
    // $scope.$on('$locationChangeSuccess', function (event, param, param1, param2) {
    //     console.log('>>', event, param, param1, param2);
    // });

    $rootScope.go = function (path) {
        $location.path(path);
        $scope.$apply()
    };

    const locationSearch = new FormData();
    locationSearch.append("params", window.location.search);

    fetch("/api/init",
        {
            method: 'post',
            body: locationSearch
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            $rootScope.userInfo = data.result;

            if ($rootScope.userInfo.viewer_type > 2) {
                console.log(">> public admin", $rootScope.userInfo.viewer_type);
                $rootScope.go('/add-app');
            } else {
                console.log(">> no admin", $rootScope.userInfo.viewer_type);
                $rootScope.go('/user-signup');
            }
        }).catch(function () {
        $rootScope.go('/add-app');
    });


    $scope.signin = function () {
        if ($rootScope.userInfo.events.length) {
            const attendData = new FormData();
            attendData.append("token", $rootScope.userInfo.token);
            attendData.append("event_id", $rootScope.userInfo.events[0].id);

            fetch("/api/events/get",
                {
                    method: 'post',
                    body: attendData
                })
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    if (data.result.attend === 1) {
                        $rootScope.go('/play');
                    } else {
                        let attendAddData = new FormData();

                        attendAddData.append("token", $rootScope.userInfo.token);
                        attendAddData.append("attend", "1");
                        attendAddData.append("event_id", $rootScope.userInfo.events[0].id);

                        // attendAddData.append("age", $rootScope.userInfo.age);
                        // attendAddData.append("about", 1);


                        fetch("/api/events/attend",
                            {
                                method: 'post',
                                body: attendAddData
                            })
                            .then(function (response) {
                                if (response.ok) {
                                    $rootScope.go('/play');
                                }
                            }).catch(function () {
                            $rootScope.go('/user-signup');
                        });
                    }
                }).catch(function () {
                $rootScope.go('/user-signup');
            });
        }
    };
});


scotchApp.controller('matchPlayController', function ($scope, $rootScope) {
    var hint = document.createElement('div');
    hint.className = "hint";
    hint.innerHTML = 'Для выбора используйте клавиши "A" и "D"';
    document.getElementsByTagName('body')[0].appendChild(hint);

    let matchQuery = new FormData();
    $scope.companion = {id: 0};

    matchQuery.append("token", $rootScope.userInfo.token);
    matchQuery.append("event_id", $rootScope.userInfo.events[0].id);

    $rootScope.matchesData = [];
    fetch("/api/matches?token=" + $rootScope.userInfo.token + "&event_id=" + $rootScope.userInfo.events[0].id)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            $rootScope.matchesData.push(data.result.company);
            // angular.replace();
        }).catch(function (error) {
        // $rootScope.go('/user-signup');
    });


    fetch("/api/events/get",
        {
            method: 'post',
            body: matchQuery
        })
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            $scope.companion = data.result.companion;

            if (!$scope.companion.id) {
                $rootScope.go('/empty');
            } else {
                $rootScope.go('/play');
            }
        }).catch(function () {
        $rootScope.go('/user-signup');
    });

    fetch("/api/matches?token=" + $rootScope.userInfo.token + "&event_id=" + $rootScope.userInfo.events[0].id)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            $rootScope.matchesData = data.result;
        }).catch(function (error) {
        $rootScope.go('/user-signup');
    });

    $scope.estimate = function (rate) {
        let matchQuery = new FormData();

        if (!$scope.companion.id) {
            $rootScope.go('/empty');
        } else {
            matchQuery.append("token", $rootScope.userInfo.token);
            matchQuery.append("event_id", $rootScope.userInfo.events[0].id);
            matchQuery.append("like", rate);
            matchQuery.append("id", $scope.companion.id);

            fetch("/api/events/get",
                {
                    method: 'post',
                    body: matchQuery
                })
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    $scope.companion = data.result.companion;
                    $rootScope.go('/play');
                }).catch(function () {
                $rootScope.go('/user-signup');
            });
        }
    };

    document.onkeypress = function (e) {
        const rate = e.keyCode === 97 ? 0 : 1;
        $scope.estimate(rate);
    };
});
scotchApp.controller('matchController', function ($scope, $rootScope) {
    $scope.matchesDatas = [];

    fetch("/api/matches?token=" + $rootScope.userInfo.token + "&event_id=" + $rootScope.userInfo.events[0].id)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            $scope.matchesDatas = data.result.matches;
            console.log($scope.matchesDatas)
            angular.replace();
        }).catch(function (error) {
        $rootScope.go('/matches');
    });

    $scope.go = function(path){
      $rootScope.go(path);
    };
});
// scotchApp.controller('contactController', function ($scope) {
//     $scope.message = 'Contact us! JK. This is just a demo.';
// });