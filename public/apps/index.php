<!DOCTYPE html>

<html ng-app="afishaApp">

<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="./files/css/reset.css"/>
    <link rel="stylesheet" href="//necolas.github.io/normalize.css/7.0.0/normalize.css"/>
    <link rel="stylesheet" href="./files/css/main.css?<?php echo time() ?>"/>

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular-route.js"></script>
    <script src="./files/js/scripts.js?<?php echo time() ?>"></script>
</head>

<body ng-controller="mainController">
<div class="wrapper">
    <div id="userMain">
       <?php /*---<div class="header">
             <nav class="navbar navbar-default">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                    <li><a href="#admin"><i class="fa fa-shield"></i>Admin</a></li>
                    <li><a href="#user-signup"><i class="fa fa-comment"></i>Пользователь</a></li>
                </ul>
             </nav>
        </div>---*/ ?>
        <div ng-view></div>
    </div>
</div>
</body>
</html>
