<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/init', 'BaseController@init');

Route::post('/user', 'PersonController@auth');

Route::get('/events', 'EventController@get');

Route::group(['middleware' => ['auth.api']], function () {

    Route::post('/events/add', 'EventController@add');
    Route::post('/events/remove', 'EventController@remove');
    Route::post('/events/attend', 'PersonController@attend');
    Route::post('/events/get', 'PersonController@event');
    Route::get('/matches', 'DatingController@matches');

});