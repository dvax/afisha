<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 18:54
 */

namespace App\Helpers;

class Helper
{


    public static function parseUrl($string)
    {

        parse_str( parse_url( $string, PHP_URL_QUERY), $params );

        return $params;
    }

}