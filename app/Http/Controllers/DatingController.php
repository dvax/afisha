<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 21.10.17
 * Time: 11:26
 */

namespace App\Http\Controllers;


use App\Match;
use App\Person;
use App\Token;
use Illuminate\Http\Request;

class DatingController extends Controller
{

    public function next($event_id, $id)
    {

        //Считаем совпадения
        //$matches = count(Like::matches($this->device));

        //Проверяем на наличие переданных лайков
        //$this->like();

        $response = [];

        $response['match_count'] = count(Match::matches($id));

        //Получаем пару
        $randomUser = Person::getNext($event_id, $id);

        if ($randomUser != null) {

            $response['companion'] = [
                'id' => $randomUser->id,
                'vk_id' => $randomUser->social_uid,
                'photo' => $randomUser->photo,
                'title' => $randomUser->getTitle(),
                'subtitle' => $randomUser->about
            ];
        }


        return $response;

    }


    /**
     * Проверяем наличие лайка в переданном запросе, если есть - ставим лайк (дизлайк)
     * @param Request $request
     * @param $id
     */
    public function like(Request $request, $id)
    {
        if ($request->has('like') && $request->has('id')) {
            $status = $request->get('like');
            $vk_id = $request->get('id');
        } else {
            return;
        }


        Match::updateOrCreate([
            'active' => $id,
            'passive' => $request->get('id')
        ], [
            'status' => $request->get('like')
        ]);
    }


    /**
     * Взаимные лайки
     */
    public function matches(Request $request)
    {

        $session = Token::getByToken($request->get('token'));

        if (!$request->has('event_id')) return $this->error(1);


        $person = Person::where('social_uid', $session->social_id)->where('event_id', $request->get('event_id'))->first();

        $ids = Match::matches($person->id);


        $users = Person::whereIn('id', $ids)
            ->orderBy('id', 'desc')
            ->get();

        $friends = [];


        foreach ($users as $id) {

            $friend = [
                'link' => 'https://vk.com/id' . $id->social_uid,
                'photo' => $id->photo,
                'title' => $id->getTitle(),
                'subtitle' => $id->about,
            ];

            $friends[] = $friend;

        }



        $ids = Match::companies($ids, $person->id);

        $users = Person::whereIn('id', $ids)
            ->orderBy('id', 'desc')
            ->get();

        $companies = [];


        foreach ($users as $id) {

            $friend = [
                'link' => 'https://vk.com/id' . $id->social_uid,
                'photo' => $id->photo,
                'title' => $id->getTitle(),
                'subtitle' => $id->about,
            ];

            $companies[] = $friend;

        }

        return $this->result(['matches' => $friends, 'company' => $companies]);

    }


}