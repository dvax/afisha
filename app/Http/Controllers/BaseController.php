<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 20:32
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BaseController extends Controller
{

    public function init(Request $request){


        $personController = new PersonController();
        $eventController = new EventController();

        $auth = $personController->auth($request);


        $auth = json_decode($auth, true);

        if (isset($auth['result'])) {

            $events = json_decode($eventController->get($request), true);

            $auth['result']['events'] = $events['result'];

            return $this->result($auth['result']);
        }
        else{
            return $this->error(401);
        }
    }

}