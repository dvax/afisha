<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 18:08
 */

namespace App\Http\Controllers;


use App\Helpers\Helper;
use App\Libs\VKLib;
use App\Person;
use App\Token;
use Illuminate\Http\Request;

class PersonController extends Controller
{


    /**
     * @param Request $request
     * @return string
     */
    public function auth(Request $request)
    {

        $params = Helper::parseUrl($request->get('params'));


        if (!VKLib::checkResponse($params))
            return $this->error(401);

        $user = VKLib::apiResult($params);
        $user = $user[0];

        $data = [];
        $data['viewer_type'] = $params['viewer_type'];
        $data['photo_200'] = $user->photo_200;

        $data['token'] = $params['access_token'];

        $data['name'] = $user->first_name;

        if (isset($user->bdate)) {
            $data['age'] = Person::getAge(strtotime($user->bdate));
        } else {
            $data['age'] = null;
        }

        Token::createSession($data['name'], $data['token'], $params['viewer_id'], "", $data['age'], $params['group_id'], $data['viewer_type'],
            $user->photo_200);

        return $this->result($data);

    }


    public function event(Request $request)
    {

        $session = Token::getByToken($request->get('token'));

        if (!$request->has('event_id')){
            return $this->error(1);
        }

        $event_id = $request->get('event_id');
        $user_id = $session->social_id;

        $person = Person::where('social_uid', $user_id)->where('event_id', $event_id)->first();

        $data = [];

        if ($person == null){
            $data['attend'] = 0;
            return $this->result($data);
        }

        $data['attend'] = $person->status;

        if ($person->status == 1){

            $dating = new DatingController();

            $dating->like($request, $person->id);

            $data = array_merge($data, $dating->next($event_id, $person->id));

        }

        return $this->result($data);
    }


    public function attend(Request $request)
    {


        if (!$request->has('event_id') || !$request->has('attend')) {
            return $this->error(1);
        }


        $session = Token::getByToken($request->get('token'));

        $event_id = $request->get('event_id');
        $attend = $request->get('attend');

        if ($request->has('about')) {
            $about = $request->get('about');
        } else {
            $about = "";
        }

        if ($request->has('age')){
            $age = $request->get('age');
        }
        elseif ($session->birthday > 0){
            $age = $session->birthday;
        }
        else{
            $age = "";
        }

        Person::updateOrCreate([
            'event_id' => $event_id,
            'social_uid' => $session->social_id
        ], [
            'status' => $attend,
            'about' => $about,
            'name' => $session->name,
            'subtitle' => $age,
            'photo' => $session->photo
        ]);

        return $this->result(true);
    }

}