<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 20:00
 */

namespace App\Http\Controllers;


use App\Event;
use App\Helpers\Helper;

use App\Token;
use Illuminate\Http\Request;

class EventController extends Controller
{

    public function get(Request $request)
    {
        $data = Helper::parseUrl($request->get('params'));

        $social_id = $data['group_id'];

        $events = Event::where('social_id', $social_id)->get()->toArray();

        return $this->result($events);

    }

    public function add(Request $request)
    {

        $token = $request->get('token');

        $session = Token::getByToken($token);

        if ($session->viewer_type >= 3) {
            $event = new Event();

            $event->title = $request->get('title');
            $event->description = $request->get('description');
            $event->social_id = $session->social_gid;
            $event->token = "";

            $event->save();

            return $this->result(true);
        }
        else{
            return $this->result(false);
        }


    }

    public function remove(Request $request)
    {
        $token = $request->get('token');

        $session = Token::getByToken($token);

        if ($session->viewer_type >= 3 && $request->has('id')) {
            Event::destroy($request->get('id'));
            return $this->result(true);
        }
        else{
            return $this->result(false);

        }
    }
}