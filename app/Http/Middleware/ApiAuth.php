<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->has('token')) return response('Unauthorized.', 401);

        $token = $request->get('token');

        $session = Token::getByToken($token);


        if ($session != null) {
            return $next($request);
        }
        else{
            return response('Unauthorized.', 401);
        }
    }
}
