<?php
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 21:23
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Token extends Model
{

    protected $table = 'tokens';

    protected $fillable = ['token', 'name', 'social_id', 'occupation', 'birthday', 'social_gid', 'viewer_type', 'photo'];


    public static function createSession($name, $token, $social_id, $occupation, $birthday, $gid, $viewer, $photo)
    {

        Token::firstOrCreate([
            'token' => $token,
            'social_id' => $social_id], [
            'name' => $name,
            'occupation' => $occupation,
            'birthday' => $birthday,
            'social_gid' => $gid,
            'viewer_type' => $viewer,
            'photo' => $photo
        ]);

    }


    public static function getByToken($token)
    {
        return Token::where('token', $token)->first();
    }

}