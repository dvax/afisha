<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    protected $table = 'persons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'subtitle', 'about', 'social_uid', 'event_id', 'status', 'photo'
    ];


    public static function fromRequest($params, $event_id, $user_data = null)
    {

    }

    public function getTitle(){
        return $this->name . ', ' . $this->subtitle;
    }

    public static function getAge($bdate)
    {
        $bdate = date('d-m-Y', $bdate);

        $date = new DateTime($bdate);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    public static function getNext($event_id, $id)
    {

        $voted = Match::getVoted($id);

        $voted[] = $id;

        $randomUser = Person::where('event_id', $event_id)
            ->where('status', 1)
            ->whereNotIn('id', $voted)
            ->inRandomOrder();

        $randomUser = $randomUser->first();

        return $randomUser;

    }
}
