<?php

namespace App\Libs;
/**
 * Created by PhpStorm.
 * User: svatoslavpavlov
 * Date: 20.10.17
 * Time: 17:10
 */

class VKLib
{

    const SECRET_KEY = 'gt1yXEsl2VU9bQFnryXV';

    public function __construct($key)
    {

    }


    public static function checkResponse($params)
    {

        $sign = "";

        foreach ($params as $key => $param) {

            if ($key == 'hash' || $key == 'sign' || $key == 'api_result') continue;

            $sign .= $param;

        }

        $secret = self::SECRET_KEY;

        $sig = $secret ? hash_hmac('sha256', $sign, $secret) : "";

        return $sig === $params['sign'];

    }


    public static function apiResult($params){
        $response = json_decode($params['api_result']);
        return $response->response;
    }

}