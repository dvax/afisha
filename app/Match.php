<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Match extends Model
{

    protected $table = 'matches';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active', 'passive', 'status'
    ];

    protected $hidden = ['created_at', 'updated_at'];


    public static function getVoted($id, $type = 1, $status = -1)
    {

        if ($type == 1) {

            if ($status == -1) {
                return Match::where('active', $id)->get()->pluck('passive')->toArray();
            } else {
                return Match::where('active', $id)->get()->pluck('passive')->toArray();
            }
        } else {

            if ($status == -1) {
                return Match::where('passive', $id)->get()->pluck('active')->toArray();
            } else {
                return Match::where('passive', $id)->where('status', $status)->get()->pluck('active')->toArray();
            }
        }

    }


    public static function matches($id)
    {


        $my_likes = Match::getVoted($id, 1, 1);

        //var_dump($my_likes);

        $peoples_like = Match::getVoted($id, 2, 1);
        //var_dump($peoples_like);

        $ids = array_intersect($peoples_like, $my_likes);

        return $ids;


    }


    public static function companies($ids, $user_id)
    {

        $points = [];

        foreach ($ids as $id) {

            $matches = self::matches($id);

            $intersect = array_intersect($matches, $ids);

            if (count($intersect) > 0) {
                foreach ($intersect as $iid) {

                    if (!isset($points[$iid])) {
                        $points[$iid] = 0;
                    }

                    $points[$iid]++;
                }
            }

        }


        unset($points[$user_id]);

        arsort($points);
        reset($points);

        $points = array_keys($points);

        return array_slice($points, 0, 5,true);

    }
}
