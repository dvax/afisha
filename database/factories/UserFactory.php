<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


;

$factory->define(App\Person::class, function (Faker $faker){

    return[
        'social_uid' => $faker->numberBetween(1, 18000),
        'photo' => $faker->imageUrl(200, 200, 'people'),
        'name' => $faker->name,
        'subtitle' => $faker->numberBetween(18, 30),
        'about' => $faker->city . ", ".$faker->company,
        'status' => 1,
        'event_id' => $faker->numberBetween(1, 20)
    ];

});
